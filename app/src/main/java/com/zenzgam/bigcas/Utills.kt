package com.zenzgam.bigcas

import android.content.Context
import android.webkit.JavascriptInterface

private const val BIG_TABLE = "com.BIG.table.111"
private const val BIG_ARGS = "com.BIG.value.222"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(BIG_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(BIG_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(BIG_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(BIG_ARGS, null)
}
